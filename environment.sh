#!/bin/bash


ENVIRONMENTS=(dev preprod prod)
unset environment


if [[ ${#} -lt 1 ]]; then

   echo "usage:   source ./environment.sh [${ENVIRONMENTS[*]}]"
   echo "example: source ./environment.sh ${ENVIRONMENTS[0]}"
   return 1
else
   for env in ${ENVIRONMENTS[@]}; do
       if [[ "${env}" == "${1}" ]]; then
           environment=${1}
        export ANSIBLE_PROJECT_ENVIRONMENT=${environment}
    fi
   done
fi

export ANSIBLE_DIR="$(dirname $(readlink -f ${BASH_SOURCE[0]}))"

## ANSIBLE HOSTS
export ANSIBLE_INVENTORY="${ANSIBLE_DIR}/etc/${environment}.inventory"

## ANSIBLE CONF FILE
export ANSIBLE_CONFIG="${ANSIBLE_DIR}/etc/ansible.cfg"

## Project name used by VAULT python script
export ANSIBLE_PROJECT_NAME=$(basename $(echo "${ANSIBLE_DIR}" |sed 's/ansible//g' |sed 's/-deployment//g'))

env |grep -E 'ANSIBLE'
